package com.simplilearn.flight.flyaway;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.simplilearn.flight.flyaway.entity.FlightBooking;
import com.simplilearn.flight.flyaway.entity.Passenger;
import com.simplilearn.flight.flyaway.entity.dao.FlightBookingDAO;
import com.simplilearn.flight.flyaway.entity.dao.PassengerDAO;
@Path("/flightbooking")
public class FlightBookingResource {
	@GET
    @Produces("application/json")
    public List<FlightBooking> getFlightBooking() {
        FlightBookingDAO dao = new FlightBookingDAO();
        //Passenger passenger=new Passenger();
        List FlightBooking = dao.getFlightBooking();
        return FlightBooking;
    }
 
	

    @POST
    @Path("/create")
    @Consumes("application/json")
    public Response addFlightBooking(FlightBooking flightbooking){
              flightbooking.setId(flightbooking.getId());   
              //passenger.setId(passenger.getId());
             
              //flightbooking.setPassenger_id(flightbooking.getPassenger_id());
             
        FlightBookingDAO dao = new FlightBookingDAO();
       //// PassengerDAO dao1=new PassengerDAO();
        dao.addFlightBooking(flightbooking);
        //dao1.addPassenger(passenger);
        
        return Response.ok().build();
    }
    
    @PUT
    @Path("/update/{id}")
    @Consumes("application/json")
    public Response updateFlightBooking(@PathParam("id") int id, FlightBooking flightbooking){
        FlightBookingDAO dao = new FlightBookingDAO();
        PassengerDAO dao1=new PassengerDAO();
        int count = dao.updateFlightBooking(id, flightbooking);
        //int count1=dao1.updatePassenger(id, passenger);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
    
    @DELETE
    @Path("/delete/{id}")
    @Consumes("application/json")
    public Response deleteFlightBooking(@PathParam("id") int id){
        FlightBookingDAO dao = new FlightBookingDAO();
        int count = dao.deleteFlightBooking(id);
        if(count==0){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }
}


package com.simplilearn.flight.flyaway.entity;

import java.util.ArrayList;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.ws.rs.Path;
import javax.persistence.JoinColumn;


@Entity
@Table(name="FlightBooking")

public class FlightBooking {

	@Id
	private int id;
	//private int passenger_id;
	//@Column(name="id")

	@ManyToOne
	private Passenger passenger;

	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "booking_flights", joinColumns = @JoinColumn(name = "booking_id"), inverseJoinColumns = @JoinColumn(name = "flight_id"))    
	private Set<Flight> flights;

	
	
public FlightBooking(){};
	
	//add constructor with all fields
	public FlightBooking(int id) {
		super();
		this.id=id;
		//this.passenger_id=passenger_id;
	}
	

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Set<Flight> getFlights() {
		return flights;
	}

	public void setFlights(Set<Flight> flights) {
		this.flights = flights;
	}

	@Override
	public String toString() {
		return "FlightBooking [id=" + id +"]";
	}
	
	//add helper method for bidirectional rel.
		public void add(Flight flight) {
			if(flights ==null) {
				flights =  (Set<Flight>) new ArrayList<Flight>();
			}
			flights.add(flight);
		}

}
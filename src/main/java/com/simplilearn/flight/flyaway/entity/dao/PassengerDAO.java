package com.simplilearn.flight.flyaway.entity.dao;
//package com.simplilearn.flight.flyaway.entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.time.LocalDateTime;

import com.simplilearn.flight.flyaway.entity.Airport;
import com.simplilearn.flight.flyaway.entity.Flight;
import com.simplilearn.flight.flyaway.entity.Passenger;

import com.simplilearn.flight.flyaway.entity.util.SessionUtil;

public class PassengerDAO {

		
		public void addPassenger(Passenger bean){
	        Session session = SessionUtil.getSession();        
	        Transaction tx = session.beginTransaction();
	        addPassenger(session,bean);        
	        tx.commit();
	        session.close();
	        
	    }
	    
	    private void addPassenger(Session session, Passenger bean){
	        Passenger passenger = new Passenger();
	        passenger.setId(bean.getId());
	        passenger.setFirstName(bean.getFirstName());
	        passenger.setLastName(bean.getLastName());
	        passenger.setEmail(bean.getEmail());
	        passenger.setPassword(bean.getPassword());
	        //passenger.setArrivalDate(bean.getArrivalDate());
	                
	        session.save(passenger);
	    }
	    
	    public List<Passenger> getPassenger(){
	        Session session = SessionUtil.getSession();    
	        Query query = session.createQuery("from Passenger");
	        List<Passenger> passenger =  query.list();
	        session.close();
	        return passenger;
	    }
	    
	    public Passenger getPassengerByEmail(Passenger passenger) {
	    	Session session = SessionUtil.getSession();    
	    	Query query = session.createQuery("from Passenger where email=:email");
	    	query.setParameter("email", passenger.getEmail());
	    	Passenger passengers =  (Passenger)query.getResultList().get(0);
	    	session.close();
	    	return passengers;
	    }
	 
	    public int deletePassenger(int id) {
	        Session session = SessionUtil.getSession();
	        Transaction tx = session.beginTransaction();
	        String hql = "delete from Passenger where id = :id";
	        Query query = session.createQuery(hql);
	        query.setInteger("id",id);
	        int rowCount = query.executeUpdate();
	        System.out.println("Rows affected: " + rowCount);
	        tx.commit();
	        session.close();
	        return rowCount;
	    }
	    
	    public Passenger loginPassenger(Passenger passenger) {
	    	Session session = SessionUtil.getSession();    
	    	Query query = session.createQuery("from Passenger where email=:email and password=:password");
	    	query.setParameter("email", passenger.getEmail());
	    	query.setParameter("password",passenger.getPassword());
	    	Passenger passengers =  (Passenger)query.getResultList().get(0);
	    	session.close();
	    	return passengers;
	    }
	    
	    public int updatePassenger(int id, Passenger passenger){
	         if(id <=0)  
	               return 0;  
	         Session session = SessionUtil.getSession();
	            Transaction tx = session.beginTransaction();
	            String hql = "update Passenger set firstname = :firstname, lastname=:lastname, email=:email, password=:password where id=:id";
	            Query query = session.createQuery(hql);
	            
	            query.setInteger("id",id);
	            query.setString("firstname",passenger.getFirstName());
	            query.setString("lastname",passenger.getLastName());
	            query.setString("email",passenger.getEmail());
	            query.setString("password", passenger.getPassword());
	            //query.setString("arrivaldate",flight.getArrivalDate());
	            
	            int rowCount = query.executeUpdate();
	            System.out.println("Rows affected: " + rowCount);
	            tx.commit();
	            session.close();
	            return rowCount;
	    }

	
		

	}


